import { products_id } from './data.js'

let header = document.querySelector('header')

if (!localStorage.cart) localStorage.cart = []
if (!localStorage.liked) localStorage.liked = []

console.log(localStorage.liked.split(',').length);

let under_header = document.querySelector('div.none_search')

under_header.innerHTML = `<div class="none_search"><form name="register"><input type="text" placeholder="Введите название товара"><button><img src="./static/Picture/search.svg" alt="search"></button></form></div>`

header.innerHTML = `
<div class="logo">
<img src="./static/Picture/logo.jpg" height="50px" class="logo_none" alt="logo">
<h1>
    <a href="./index.html">
        Wesell
    </a>
</h1>
</div>
<form name="register">
    <input id="search-element" type="text" placeholder="Введите название товара">
    <button><img src="./static/Picture/search.svg" alt="search"></button>
</form>
<div class="link">
<img src="./static/Picture/credit-card.svg" class="none_card" alt="credit-card">
<a href="./favorite.html" class="favorite-link">
    <nav id="menu">
        <div class="menu-item">
            <div class="menu-text">
                <a href="./favorite.html"><img src="./static/Picture/heart.svg" alt=""></a>
                <div class="heart-num">${localStorage.liked.split(',').length}</div>
            </div>
            <div class="sub-menu heart_fav">
                
                <div class="sub-menu-holder"></div>
            </div>
        </div>
    </nav>
</a>
<a href="./Cart.html" class="cart-link ">
    <nav id="menu">
        <div class="menu-item">
            <div class="menu-text">
                <a href="./Cart.html"><img src="./static/Picture/shopping-bag.svg" alt=""></a>
                <div class="cart-num heart-num">${localStorage.cart.split(',').length}</div>
            </div>
            <div class="sub-menu fav-num">
                <div class="sub-menu-holder"></div>
            </div>
        </div>
    </nav>
</a>
</div>
<div class="profile">
<div class="name-money">
    <p name="name">name</p>
    <p name="money">money</p>
</div>
<div class="profile-logo">
<a href="./auth.html">
<img src="./static/Picture/Face.png" alt="">
</a>
</div>
</div>
        
`
let name_prof = document.querySelector('p[name="name"]')
let money_prof = document.querySelector('p[name="money"]')

name_prof.innerHTML = "Alex Adams"
money_prof.innerHTML = "$" + 5000

let menu_fav = document.querySelector('.heart_fav')
let fav_num = document.querySelector('.fav-num')

let reload_prod_menu_fav = (details) => {

    menu_fav.innerHTML = ''

    for (let item of details) {
        let a_fav = document.createElement('a')
        let div_fav_text = document.createElement('div')
        let div_fav_div_img = document.createElement('img')
        let div_fav_p_name = document.createElement('h3')
        let div_fav_p_price = document.createElement('p')
        let div_text = document.createElement('div')

        div_fav_p_name.innerHTML = item.name
        div_fav_p_price.innerHTML = '$' + item.price


        for (let img of item.images) {
            div_fav_div_img.setAttribute('src', `${img}`)
        }

        a_fav.setAttribute('href', `./product.html?id=${item.id}`)
        a_fav.setAttribute('target', '_blank')

        div_fav_div_img.classList.add('img_prod')
        div_fav_text.classList.add("text")
        a_fav.classList.add('icon-box')


        div_text.append(div_fav_p_name, div_fav_p_price)
        div_fav_text.append(div_fav_div_img, div_text)
        a_fav.append(div_fav_text)
        menu_fav.append(a_fav)
    }
}

reload_prod_menu_fav(products_id.filter(item => item.isFavorite == true)
)

let reload_prod_menu_cart = (details) => {

    fav_num.innerHTML = ''

    for (let item of details) {
        let a_fav = document.createElement('a')
        let div_fav_text = document.createElement('div')
        let div_fav_div_img = document.createElement('img')
        let div_fav_p_name = document.createElement('h3')
        let div_fav_p_price = document.createElement('p')
        let div_text = document.createElement('div')

        div_fav_p_name.innerHTML = item.name
        div_fav_p_price.innerHTML = '$' + item.price


        for (let img of item.images) {
            div_fav_div_img.setAttribute('src', `${img}`)
        }

        a_fav.setAttribute('href', `./product.html?id=${item.id}`)
        a_fav.setAttribute('target', '_blank')

        div_fav_div_img.classList.add('img_prod')
        div_fav_text.classList.add("text")
        a_fav.classList.add('icon-box')


        div_text.append(div_fav_p_name, div_fav_p_price)
        div_fav_text.append(div_fav_div_img, div_text)
        a_fav.append(div_fav_text)
        fav_num.append(a_fav)
    }
}

reload_prod_menu_cart(products_id.filter(item => item.isInCart == true)
)