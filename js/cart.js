import {
   products_id,
   mapped_categories
} from './data.js'
let cart_mid = document.querySelector('.cart-mid')
let checkAll = document.querySelector("#checkAll")
let table_img = document.querySelector(".table")
let boolProducts = false
let bool = false

table_img.onclick = () => {
   boolProducts = !boolProducts
   if (boolProducts == true) {
      table_img.setAttribute('src', "./static/Picture/grid.png")
      cart_mid.style.gridTemplateColumns = `repeat(1, 1fr)`
   } else {
      table_img.setAttribute('src', "./static/Picture/block.png")
      cart_mid.style.gridTemplateColumns = `repeat(2, 1fr)`
   }
}

let heart_num = document.querySelector('.heart-num')

// onclick favorite replay
const addFarvorite = (id) => {
   let find = products_id.filter(item => item.id == id)[0]
   find.isFavorite = !find.isFavorite
   reload(products_id)
   heart_num.innerText = products_id.filter(item => item.isFavorite == true).length
}

heart_num.innerText = products_id.filter(item => item.isFavorite == true).length
// del favotrite
const delFavorite = (id) => {
   let find = products_id.filter(item => item._id == id)[0]
   find.isFavorite = false
   reload(products_id)
}
// reload
const reload = (arr) => {
   cart_mid.innerHTML = ""
   let IsFavorite = arr.filter(item => item.isFavorite == true).length

   if (IsFavorite == 0) {
      let noneElem = document.createElement('p')
      noneElem.innerText = "Нажмите на сердечку"
      cart_mid.append(noneElem)
   }

   for (const item of arr) {
      item._id = arr.indexOf(item)

      if (item.isFavorite == true) {
         // create elem
         let div_item = document.createElement('div')
         let img_favorite = document.createElement('img')
         let a_item = document.createElement('a')
         let div_content = document.createElement('div')
         let img_src = document.createElement('img')
         let a_content = document.createElement('a')
         let div_price = document.createElement('div')
         let div_change = document.createElement('div')
         let btn_change = document.createElement('button')
         // add class
         div_item.classList.add('cart-mid-item')
         img_favorite.classList.add('favorite-img')
         a_item.classList.add('img')
         div_content.classList.add('content')
         a_content.classList.add('title')
         div_price.classList.add('price')
         div_change.classList.add('change')
         btn_change.classList.add('btn', "favorite")
         // add attribute
         img_favorite.setAttribute('src', "./static/Picture/favorite_black.png")
         a_item.setAttribute('href', "#")
         a_content.setAttribute('href', "#")
         img_src.setAttribute('src', item.images)
         // price name
         a_content.innerText = item.name
         div_price.innerText = `${item.price}$`
         btn_change.innerText = "Убрать с корзины"
         // appen
         div_change.append(btn_change)
         div_content.append(a_content, div_price, div_change)
         a_item.append(img_src)
         div_item.append(img_favorite, a_item, div_content)
         cart_mid.append(div_item)
         // onclick
         btn_change.onclick = () => {
            delFavorite(item._id)
         }
         btn_change.onclick = () => {
            event.preventDefault()
            addFarvorite(item.id)
         }
      }
   }
}
reload(products_id)
// active
checkAll.onclick = () => {
   let div_active = document.querySelectorAll('.cart-mid-item')
   bool = !bool
   for (const item of div_active) {
      if (bool == true) {
         item.classList.add('active')
      } else {
         item.classList.remove('active')
      }
   }
}