let categories = [{
        id: 1,
        title: 'Телевизоры',
        count: 0
    },
    {
        id: 2,
        title: 'Телефоны',
        count: 0
    },
    {
        id: 3,
        title: 'Аксессуары',
        count: 0
    },
    {
        id: 4,
        title: 'Ноутбуки',
        count: 0
    },
]

let products = [{
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 1500,
        sale: 0,
        description: '100% цветовой объем благодаря технологии квантовых точек',
        comements: [{
            user: 'Blin',
            text: 'worth the money'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://avatars.mds.yandex.net/i?id=baab7d55834681f162190fe8463c8aa8-5254427-images-thumbs&ref=rim&n=33&w=157&h=150'],
        category: 1,
    },
    {
        name: ' QLED 8K Smart TV 2021',
        price: 2500,
        sale: 0,
        description: 'Экран 4K UHD телевизора содержит в 4 раза больше пикселей по сравнению с экраном FHD телевизора',
        comements: [{
            user: 'alex',
            text: 'expensive'
        }],
        isFavorite: true,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/qe65q60abuxru/gallery/ru-qled-q60ab-qe65q60abuxru-530351662?$684_547_PNG$'],
        category: 1,

    },
    {
        name: 'Neo 8K Smart TV 2021',
        price: 2400,
        sale: 0,
        description: 'Узкая рамка - больше площадь экрана',
        comements: [{
            user: 'alex',
            text: 'very steep'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://avatars.mds.yandex.net/i?id=2443a82676da880efbdc078f0ded194c-5289254-images-thumbs&ref=rim&n=33&w=226&h=150'],
        category: 1,

    },
    {
        name: 'Crystal UHD 4K Smart TV AU8070 Series 8',
        price: 2000,
        sale: 0,
        description: 'Улучшенная плавность движения для четкости изображения Функция Motion Xcelerator',
        comements: [{
            user: 'Andrea',
            text: 'excellent TV'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/qe65q60abuxru/gallery/ru-qled-q60ab-qe65q60abuxru-530351662?$684_547_PNG$'],
        category: 1,

    },
    {
        name: 'Q80A QLED 4K Smart ',
        price: 1400,
        sale: 0,
        description: 'Насыщенные цвета даже в темных сценах благодаря HDR 10',
        comements: [{
            user: 'Tony',
            text: 'not bad'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://i5.walmartimages.com/asr/49613973-9050-471e-8bc6-687bd53235f3.bc1d96a59a5e2c0d6ccb75b427ee395f.jpeg'],
        category: 1,

    },
    {
        name: 'UHD 4K Smart TV AU7500 Series 7',
        price: 2000,
        sale: 0,
        description: 'Оптимизированное качество изображения с технологией PurColor',
        comements: [{
            user: 'Frank',
            text: 'super quality'
        }],
        isFavorite: true,
        isInCart: false,
        sale_time: 24,
        images: ['https://m.openshop.uz/public/uploads/products/photos/OmD9JRW9waicmyHumotkUZiR5Lt1IdUFTDUebJ5R.jpeg'],
        category: 1,

    },
    {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 2000,
        sale: 0,
        description: 'Технология Q-Symphony',
        comements: [{
            user: 'John',
            text: 'WOW, something new'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://avatars.mds.yandex.net/i?id=fbe4af5cac0bbf37de87af30d0ddae90-5219700-images-thumbs&n=13'],
        category: 1,

    },
    {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 3000,
        sale: 0,
        description: 'Масштабирование контента до 8К',
        comements: [{
            user: 'Daler',
            text: '3000 is too expensive'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/qe65q60abuxru/gallery/ru-qled-q60ab-qe65q60abuxru-530351662?$684_547_PNG$'],
        category: 1,
    }, {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 2200,
        sale: 0,
        description: 'Технология двойной подсветки Dual LED',
        comements: [{
            user: 'Jasur',
            text: 'very bright and beautiful colors'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://www.bhphotovideo.com/images/images2500x2500/samsung_un32m4500bfxza_m4500_series_32_smart_1395973.jpg'],
        category: 1,

    },
    {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 2000,
        sale: 0,
        description: 'Антибликовое покрытие',
        comements: [{
            user: 'Adams',
            text: 'wow, the kit comes with accessories thats cool'
        }],
        isFavorite: true,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/qe65q60abuxru/gallery/ru-qled-q60ab-qe65q60abuxru-530351662?$684_547_PNG$'],
        category: 1,

    },
    {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 2000,
        sale: 0,
        description: 'Технология квантовых точек',
        comements: [{
            user: 'Morgen',
            text: 'GG'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/ue55au8040uxru/gallery/ru-crystal-uhd-au8000-390992-ue55au8040uxru-455121067?$684_547_PNG$'],
        category: 1,

    },
    {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 2000,
        sale: 0,
        description: 'Режим отображения нескольких экранов Multi View',
        comements: [{
            user: 'Oleg',
            text: 'GG'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/qe85qn85aauxru/gallery/ru-neo-qled-qn85a-384822-qe85qn85aauxru-426222217?$684_547_PNG$'],
        category: 1,

    },
    {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 2000,
        sale: 0,
        description: 'One Connect Box',
        comements: [{
            user: 'Justin',
            text: 'GG'
        }],
        isFavorite: true,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/qe85qn85aauxru/gallery/ru-neo-qled-qn85a-384822-qe85qn85aauxru-426222222?$684_547_PNG$'],
        category: 1,

    },
    {
        name: 'Q60AB QLED 4K Smart TV 2021',
        price: 1999,
        sale: 0,
        description: 'Мощный процессор Crystal 4K',
        comements: [{
            user: 'Thomas',
            text: 'GG'
        }],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://images.samsung.com/is/image/samsung/p6pim/ru/qe75q80aauxru/gallery/ru-qled-q80a-qe75q80aauxru-426998250?$684_547_PNG$'],
        category: 1,

    },
    {
        name: 'Ноутбук HP Pavilion 15-ba006ur',
        price: 230,
        sale: 10,
        description: 'HP 15-ba006ur, на базе процессора AMD E-Series имеет прекрасное соотношение цена - качество. Благодаря видеокарте AMD Radeon R2 series и оперативной памяти, объемом 4 ГБ модель 15-ba006ur будет идеальным вариантом ноутбука для офиса и дома. Автономная работа до 3 часов, что позволяет работать длительное время без питания, для хранения данных, пользователям предоставлено 500 ГБ на жестком диске.',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 48,
        images: [
            'pxel.ru/images/products/1aabb968c796bf3b1d152a0c626f626e.jpg',
            'https://www.notik.ru/goods/notebooks-hp-15-ba006ur-black-46688.htm#',
            'https://www.pxel.ru/images/products/879c407d43ee48746de12886a903dafe.jpg',
        ],
        category: 4,
    },
    {
        name: 'Ноутбук Acer Extensa EX2511G-323A ',
        price: 380,
        sale: 11,
        description: 'Оснащен 15.6-дюймовым экраном, обладает современным процессором Intel Core i3 2000 МГц Broadwell (5005U), имеет небольшой вес, а также высокопроизводительную видеокарту NVIDIA GeForce 940M. Модель Extensa EX2511G-323A будет привлекательна для покупателей, часто пользующихся ноутбуком как в помещении, так и вне его. Гарантия на ноутбук Acer EX2511G составляет 1 год. Обслуживание производится в авторизованных сервисных центрах на территории России. Данная модель находится в ценовой категории около 33 тысяч рублей. Ноутбуки Аcer пользуются стабильным спросом благодаря широкому выбору конфигураций, приемлемой цене и высочайшему качеству продукции.',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 72,
        images: [
            'https://www.pxel.ru/images/products/c19fe7426a397a966b4a7bef519dabd6.jpg',
            'https://www.pxel.ru/images/products/.tmb/thumb_c19fe7426a397a966b4a7bef519dabd6_resizePercentnew_95_71.jpg',
            'https://www.pxel.ru/images/products/.tmb/thumb_57140380d8d0e939f572e2a5636a4870_resizePercentnew_95_71.jpg'
        ],
        category: 4,
    },
    {
        name: 'Ноутбук Lenovo IdeaPad G700 LENOVOG700ATBKTX2030M4G500RRU BLACK',
        price: 324,
        sale: 35,
        description: 'Lenovo IdeaPad G700 - это стильный и современный ноутбук, отличающийся впечатляющим набором характеристик, позволяющих ему конкурировать с настольными ПК. Модель G700 комплектуется двухъядерным процессором Intel Pentium Dual-Core 2030M и дискретной графикой NVIDIA GeForce GT 720M, обеспечивающей достаточный уровень производительности для работы с ресурсоемкими приложениями и современными играми. На 17.3-дюймовом экране, оснащенном светодиодной подсветкой, одинаково комфортно работать с текстом, графикой, а также смотреть фильмы в формате высокой четкости и наслаждаться потрясающими спецэффектами в играх.',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 24,
        images: [
            'https://www.pxel.ru/images/products/d49d0ba22344e8cfed410beccdc979b6.jpg',
            'https://www.pxel.ru/images/products/.tmb/thumb_d49d0ba22344e8cfed410beccdc979b6_resizePercentnew_95_71.jpg',
            'https://www.pxel.ru/images/products/.tmb/thumb_f92f1850a8976ddadeb7eaf1e44746e1_resizePercentnew_95_71.jpg'
        ],
        category: 4,
    },
    {
        name: 'Ноутбук Asus X553SA-XX137T Black',
        price: 237,
        sale: 0,
        description: 'Asus X553SA-XX137T имеет процессор Intel Celeron Dual Core 1600 МГц, а так же видеокарту Intel HD Graphics, в сочетании с 15.6 дюймовым экраном, пользователям доступны максимальные мультимедийные возможности. Ноутбук Asus X553SA-XX137T, рассчитан на широкий круг аудитории, в том числе игровой и бизнес класса. На хранение данных и мультимедии отведен жесткий диск, емкостью 500 ГБ, это позволяет всегда иметь под рукой всю необходимую информацию и файлы, не зависимо от места нахождения. Автономная работа данной модели предполагает до 3 часов. Гарантия на ноутбук Asus X553SA-XX137T составляет 1 год.',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 48,
        images: [
            'https://www.pxel.ru/images/products/e19007687230b6cbaba7f54de1d807a0.jpg',
            'https://www.pxel.ru/images/products/.tmb/thumb_e19007687230b6cbaba7f54de1d807a0_resizePercentnew_95_71.jpg',
            'https://www.pxel.ru/images/products/.tmb/thumb_c8bbfa60e0184147e46395855590e7e1_resizePercentnew_95_71.jpg'
        ],
        category: 4,
    },
    {
        name: 'Apple MacBook Air 13 Late 2020 ',
        price: 1000,
        sale: 75,
        description: '  Apple MacBook Air 13 Late 2020 (2560x1600, Apple M1 3.2 ГГц, RAM 8 ГБ, SSD 256 ГБ, Apple graphics 7-core):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 8,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/27fe579108f71d6a58cc0ddbdf3b2930/450/450/1999854/',
            'https://htstatic.imgsmail.ru/pic_original/b85f2e130131e9ff92673bc0ae98dd4d/2010419/',
            'https://htstatic.imgsmail.ru/pic_original/ef6f6257919baeee3b5ec9971e3b0cc1/2010422/',
        ],
        category: 4,
    },
    {
        name: 'HONOR MagicBook X 15BBR-WAH9',
        price: 237,
        sale: 0,
        description: '  HONOR MagicBook X 15BBR-WAH9 (1920x1080, Intel Core i5 1.6 ГГц, RAM 8 ГБ, SSD 512 ГБ, Win10 Home):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 48,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/83894087b3d937ae79c5673ec7b8c11e/450/450/2034364/',
            'https://htstatic.imgsmail.ru/pic_original/ae2339112220385282d59dd0c0da3e89/2034550/',
            'https://htstatic.imgsmail.ru/pic_original/04618bcdd2f5ba3afe1c9e172fdce9e2/2034551/'
        ],
        category: 4,
    },
    {
        name: 'Prestigio SmartBook 133 C4',
        price: 253,
        sale: 0,
        description: 'Prestigio SmartBook 133 C4 (1366x768, AMD A4 1.5 ГГц, RAM 4 ГБ, eMMC 64 ГБ, Win10 Pro):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 1,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/db48a23450329aa45fd2c45497ac6643/450/450/1960448/',
            'https://htstatic.imgsmail.ru/pic_original/3e8aa64acd498fb195c31ab2103de44f/1960449/',
            'https://htstatic.imgsmail.ru/pic_original/f45a076e6291d93b95d4e4cd104f3b93/1960450/'
        ],
        category: 4,
    },
    {
        name: 'DIGMA EVE 11C409',
        price: 180,
        sale: 35,
        description: '  DIGMA EVE 11C409 (1920x1080, Intel Celeron 1.1 ГГц, RAM 4 ГБ, SSD 64 ГБ, Win10 Home):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 48,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/3be0d6a73b766f2ce85bb3eb22ca994a/450/450/2027923/',
            'src="https://htstatic.imgsmail.ru/pic_original/52558fc221c6c60fc3e04071e760560e/2028001/"',
            'https://htstatic.imgsmail.ru/pic_original/2e482be652d68bdf3ba1aa16f2cb65df/2028002/'
        ],
        category: 4,
    },
    {
        name: 'Apple MacBook Air 13 Late 2020 ',
        price: 14100,
        sale: 0,
        description: '  Apple MacBook Air 13 Late 2020 (2560x1600, Apple M1 3.2 ГГц, RAM 16 ГБ, SSD 256 ГБ, Apple graphics 7-core):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 0,
        images: [
            'https://htstatic.imgsmail.ru/pic_original/e3ece6ae52614c2b3d6aac8ca50a5445/2010443/',
            'https://htstatic.imgsmail.ru/pic_original/52ad20e62a422cb6d97d9af187413420/2010440/',
            'https://htstatic.imgsmail.ru/pic_image/976b90796b0a2220f152d793d5503e2d/450/450/1999851/',
        ],
        category: 4,
    },
    {
        name: 'MSI GF63 Thin 10UD-419XRU ',
        price: 1017,
        sale: 10,
        description: '  MSI GF63 Thin 10UD-419XRU (1920x1080, Intel Core i5 2.5 ГГц, RAM 8 ГБ, SSD 512 ГБ, GeForce RTX 3050 Ti, DOS):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 80,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/a90cf6c6ad66a78ac13abcdcdd1cf7c8/450/450/2067606/',
            'https://htstatic.imgsmail.ru/pic_original/dd16b36869dd2e4b243247a9f7d0fdac/2091277/',
            'https://htstatic.imgsmail.ru/pic_original/2ff527ed517512c514ae35cad84318aa/2091278/'
        ],
        category: 4,
    },
    {
        name: 'HP PAVILION 15-eh0030ur',
        price: 747,
        sale: 99,
        description: '  HP PAVILION 15-eh0030ur (1920x1080, AMD Ryzen 5 2.3 ГГц, RAM 8 ГБ, SSD 512 ГБ, Win10 Home):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 48,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/a90cf6c6ad66a78ac13abcdcdd1cf7c8/450/450/2067606/',
            'https://htstatic.imgsmail.ru/pic_original/dd16b36869dd2e4b243247a9f7d0fdac/2091277/',
            'https://htstatic.imgsmail.ru/pic_original/2ff527ed517512c514ae35cad84318aa/2091278/'
        ],
        category: 4,
    },
    {
        name: 'HONOR MagicBook Pro',
        price: 580,
        sale: 60,
        description: '  HONOR MagicBook Pro (1920x1080, Intel Core i5 1.6 ГГц, RAM 16 ГБ, SSD 512 ГБ, GeForce MX350, Win10 Home):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 36,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/1efb55bc73997e067bb212aa3c3e0f40/450/450/1988469/',
            'https://htstatic.imgsmail.ru/pic_original/62929f9052d2767fa8388922ce4f84a5/2015678/',
            'https://htstatic.imgsmail.ru/pic_original/2796d863962892e970e971a40f333479/2015679/'
        ],
        category: 4,
    },
    {
        name: 'HP 17-by2017ur ',
        price: 428,
        sale: 50,
        description: '  HP 17-by2017ur (1600x900, Intel Pentium Gold 2.4 ГГц, RAM 8 ГБ, SSD 256 ГБ, DOS):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 72,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/0a540533520c799d0e44fb8c457b6a8b/450/450/1709570/',
            'https://htstatic.imgsmail.ru/pic_original/06b5cabc7fe77a57a5cd2408ed877fb3/1745714/',
            'https://htstatic.imgsmail.ru/pic_original/dbc0abb0a86da73e76b464f7bac42ca9/1745713/'
        ],
        category: 4,
    },
    {
        name: 'Lenovo Ideapad L340-15API',
        price: 150,
        sale: 69,
        description: '  Lenovo Ideapad L340-15API (1920x1080, AMD Ryzen 3 2.6 ГГц, RAM 4 ГБ, SSD 256 ГБ, DOS):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 69,
        images: [
            'https://htstatic.imgsmail.ru/pic_image/4d87567fa64cc1f707e402e23932d984/450/450/1987812/',
            'https://htstatic.imgsmail.ru/pic_original/88cdb46bebc8ae01bf3aeb66557a7c81/2011665/',
            'https://htstatic.imgsmail.ru/pic_original/acc6645b536ff549e89da01fb9ba9327/2011666/'
        ],
        category: 4,
    },
    {
        name: 'Lenovo IdeaPad 3 17ITL6',
        price: 237,
        sale: 50,
        description: '  Lenovo IdeaPad 3 17ITL6 (1600x900, Intel Core i3 3 ГГц, RAM 8 ГБ, SSD 256 ГБ, без ОС):  ',
        comments: [{
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
            {
                user: 'Alex Adams',
                text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nobis sint ea accusantium ad porro, voluptatibus molestias omnis nostrum, quibusdam, fuga perspiciatis tenetur ab dolorum deleniti aliquid vero tempora eos?'
            },
        ],
        isFavorite: false,
        isInCart: false,
        sale_time: 48,
        images: [
            'https://htstatic.imgsmail.ru/pic_original/e44752cfe203e11747b1597792c3f1e6/2127784/',
            'https://htstatic.imgsmail.ru/pic_original/407d2e2a55dc5f4f5bbdaad2afb3faa5/2127785/',
            'https://htstatic.imgsmail.ru/pic_image/c5aa35e41aaa81ff2cb86df1fe51ab20/450/450/2107864/',
        ],
        category: 4,
    }, {
        name: "iPhone SE",
        price: 399,
        sale: 0,
        desciption: "Lots to love. Less to spend",
        comments: [{
            user: "none",
            text: "ok"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 0,
        images: ["https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-se-white-select-2020?wid=470&hei=556&fmt=png-alpha&.v=1586574259457", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-se-black-select-2020?wid=470&hei=556&fmt=png-alpha&.v=1586574260051", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-se-red-select-2020?wid=470&hei=556&fmt=png-alpha&.v=1586574260319"],
        category: 2,
    },
    {
        name: "iPhone 11",
        price: 700,
        sale: 0,
        desciption: "As part of our efforts to reach our environmental goals, iPhone 11 does not include a power adapter or EarPods. Included in the box is a USB‑C to Lightning cable that supports fast charging and is compatible with USB‑C power adapters and computer ports.We encourage you to re‑use your current USB‑A to Lightning cables, power adapters, and headphones which are compatible with this iPhone. But if you need any new Apple power adapters or headphones, they are available for purchase.",
        comments: [{
            user: "none",
            text: "ok"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 0,
        images: ["https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone11-select-2019-family?wid=441&hei=529&fmt=jpeg&qlt=95&.v=1567022175704", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone11-green-select-2019?wid=470&hei=556&fmt=png-alpha&.v=1566956144838", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone11-yellow-select-2019?wid=470&hei=556&fmt=png-alpha&.v=1568141245782", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone11-white-select-2019?wid=470&hei=556&fmt=png-alpha&.v=1566956148115"],
        category: 2,
    },
    {
        name: "iPhone 12",
        price: 599,
        sale: 0,
        desciption: "As amazing as ever.",
        comments: [{
            user: "none",
            text: "good"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 0,
        images: ["https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-12-family-select-2021?wid=470&hei=556&fmt=jpeg&qlt=95&.v=1617135051000"],
        category: 2,
    },
    {
        name: "iPhone 12",
        price: 599,
        sale: 0,
        desciption: "As amazing as ever.",
        comments: [{
            user: "none",
            text: "good"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 0,
        images: ["https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-12-family-select-2021?wid=470&hei=556&fmt=jpeg&qlt=95&.v=1617135051000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-12-blue-select-2020?wid=470&hei=556&fmt=png-alpha&.v=1604343704000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-12-white-select-2020?wid=470&hei=556&fmt=png-alpha&.v=1604343705000"],
        category: 2,
    },
    {
        name: "iPhone 13",
        price: 699,
        sale: 0,
        desciption: "A total powerhouse.",
        comments: [{
            user: "none",
            text: "ok"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 0,
        images: ["https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-select-2021?wid=940&amp;hei=1112&amp;fmt=jpeg&amp;qlt=80&amp;.v=1631046287000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-pink-select-2021?wid=470&hei=556&fmt=png-alpha&.v=1629842709000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-blue-select-2021?wid=470&hei=556&fmt=png-alpha&.v=1629842712000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-midnight-select-2021?wid=470&hei=556&fmt=png-alpha&.v=1629907844000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-starlight-select-2021?wid=470&hei=556&fmt=png-alpha&.v=1629907845000"],
        category: 2,
    },
    {
        name: "iPhone 13 Pro",
        price: 999,
        sale: 0,
        desciption: "The ultimate iPhone.",
        comments: [{
            user: "none",
            text: "ok"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 0,
        images: ["https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-pro-family-select?wid=940&amp;hei=1112&amp;fmt=jpeg&amp;qlt=95&amp;.v=1631306948000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-pro-blue-select?wid=470&hei=556&fmt=png-alpha&.v=1631652954000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-pro-silver-select?wid=470&hei=556&fmt=png-alpha&.v=1631652954000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-pro-gold-select?wid=470&hei=556&fmt=png-alpha&.v=1631652954000", "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-pro-graphite-select?wid=470&hei=556&fmt=png-alpha&.v=1631652957000"],
        category: 2,
    },
    {
        name: "Galaxy Z Fold3 5G",
        price: 1499,
        sale: 10,
        desciption: "Все приведенные здесь спецификации и описания могут отличаться от фактических характеристик и описаний продукта. Samsung оставляет за собой право вносить изменения в этот документ и описанный в нем продукт в любое время без обязательств со стороны Samsung уведомлять о таких изменениях.",
        comments: [{
            user: "ok",
            text: "none"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 128,
        images: ["https://images.samsung.com/ru/smartphones/galaxy-z-fold3-5g/buy/zfold3_colorselection_phantomblack_mo_210719.jpg?imwidth=480", "https://images.samsung.com/ru/smartphones/galaxy-z-fold3-5g/buy/zfold3_colorselection_phantomgreen_mo_210719.jpg?imwidth=480", "https://images.samsung.com/ru/smartphones/galaxy-z-fold3-5g/buy/zfold3_colorselection_phantomsilver_mo_210719.jpg?imwidth=480"],
        category: 2,
    },
    {
        name: "Galaxy S21 Ultra 5G",
        price: 1099,
        sale: 10,
        desciption: "Все приведенные здесь спецификации и описания могут отличаться от фактических характеристик и описаний продукта. Samsung оставляет за собой право вносить изменения в этот документ и описанный в нем продукт в любое время без обязательств со стороны Samsung уведомлять о таких изменениях.",
        comments: [{
            user: "okay",
            text: "none"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 128,
        images: ["https://images.samsung.com/ru/smartphones/galaxy-s21/buy/S21_Ultra_PhantomBlack_ColorSelection_PC_img.jpg?imwidth=480", "https://images.samsung.com/ru/smartphones/galaxy-s21/buy/S21_Ultra_PhantomSilver_ColorSelection_PC_img.jpg?imwidth=480", "https://images.samsung.com/ru/smartphones/galaxy-s21/buy/S21_Ultra_PhantomTitanium_ColorSelection_PC_img.jpg?imwidth=480"],
        category: 2,
    },
    {
        name: "Galaxy Z Flip3 5G",
        price: 1199,
        sale: 20,
        desciption: "Он легко поместится в карман, сумочку или в самые узкие джинсы. А после, вы можете достать его и открыть или согнуть под нужным вам углом.Если вам показалось, что мы бросили вызов стандартным смартфонам, вам не показалось. Мы действительно сделали это!",
        comments: [{
            user: "jobs",
            text: "bad phone"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 128,
        images: ["https://images.samsung.com/ru/smartphones/galaxy-z-flip3-5g/images/galaxy-z-flip3-5g_highlights_selfie-device.jpg"],
        category: 2,
    },
    {
        name: "Xiaomi Redmi Note 8",
        price: 200,
        sale: 5,
        desciption: "Информацию об условиях отпуска (реализации) уточняйте у продавца.",
        comments: [{
            user: "Alex",
            text: "Зарядка с переходником... Очень неудобно! (("
        }, {
            user: "None",
            text: "После айфона чувствуется, что камера при фотографировании немного фокус ловит сложнее. Ну а в целом пока вопросов нет. В процессе эксплуатации посмотрим.."
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 128,
        images: ["https://avatars.mds.yandex.net/get-mpic/4397502/img_id7195576511462406352.jpeg/13hq", "https://avatars.mds.yandex.net/get-mpic/4220209/img_id1216231470706036705.jpeg/13hq", "https://avatars.mds.yandex.net/get-mpic/2008455/img_id3096709116901780515.jpeg/13hq"],
        category: 2,
    },
    {
        name: "Xiaomi Redmi 9A 2/32 ГБ Global",
        price: 150,
        sale: 10,
        desciption: "Полное погружение в виртуальный мир обеспечит новый смартфон Xiaomi Redmi 9A. Кинематографическое соотношение сторон (20:9) HD дисплея с диагональю 6,53 дюйма позволит воспроизводить любое видео. Площадь экрана визуально увеличивают тонкие рамки и совсем крошечный вырез под фронтальную камеру. Благодаря оптимизированной технологии защиты от синего цвета ваши глаза никогда не устанут от длительного пользования смартфоном. До 14 часов игр, 19 часов просмотра видео обеспечит мощная батарея емкостью 5000 мАч. Вам не нужно искать, где можно подзарядиться в течение дня. Бесперебойную работу Xiaomi Redmi 9A обеспечит фирменная система оптимизации энергопотребления. В новом гаджете срок службы аккумулятора разработчики увеличили до 2,5 лет. Надежную и стабильную работу обеспечивает процессор MediaTek Helio G25. Гаджет легко решает любую задачу.",
        comments: [{
            user: "Алексей Никулин",
            text: "Большой экран, четкий, яркий. Приличный объем ПЗУ и ОЗУ (в своей ценовой категории). Не самая плохая камера, что я встречал. При хорошем освещении, вполне приличные снимки. Хороший микрофон и динамики. Батарея очень не плохая."
        }, {
            user: "None",
            text: "Мне не нужно на смартфоне играть, а для коммуникации, ориентирования в незнакомом городе, использования нескольких нужных мне приложений и просмотра новостей возможностей этого смартфона более чем достаточно."
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 48,
        images: ["https://avatars.mds.yandex.net/get-mpic/4397559/img_id869388942241193601.jpeg/orig", "https://avatars.mds.yandex.net/get-mpic/4220209/img_id8115320370601156365.jpeg/orig", "https://avatars.mds.yandex.net/get-mpic/4944925/img_id5802912692559965472.jpeg/orig", "https://avatars.mds.yandex.net/get-mpic/4120567/img_id7434448904958153352.jpeg/orig"],
        category: 2,
    },
    {
        name: "Xiaomi Redmi 7",
        price: 130,
        sale: 10,
        desciption: `Ослепительный HD+ дисплей с каплевидным вырезом и соотношением сторон 19:9, а также с коэффициентом соотношения площади дисплея к корпусу 86.83%, позволяет видеть больше контента на нем. Дисплей защищен стеклом Corning Gorilla Glass 5, чтобы обеспечить
        его долговечность даже без защитной пленки.
        В Redmi 7 установлен высокоемкий аккумулятор на 4000 мАч, использующий технологию энергосбережения, что позволяет смартфону дольше проработать от одного заряда.
        Redmi 7 оснащен процессором Qualcomm Snapdragon 632 с 8 ядрами Kryo 250, что обеспечивает улучшение производительности на 40%* и более плавную и быструю работу устройства. Процессор Snapdragon 632 также обладает великолепной энергоэффективностью, что позволит вам пользоваться вашим устройством еще дольше. Графический процессор Qualcomm Adreno 506 сделает игровой опыт еще ярче. Redmi 7 готов удовлетворить любые потребности, независимо от того, каким образом вы будете его использовать.`,
        comments: [{
            user: "dafnyfox",
            text: "Отличная цена,со всеми скидками обошелся по самой низкой цене по всей Москве,даже Горбушка со своими нелегалами не перебила. Мама довольна."
        }, {
            user: "Lox",
            text: "Пока все нравится!"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 48,
        images: ["https://avatars.mds.yandex.net/get-mpic/4033296/img_id163612759520252929.jpeg/13hq", "https://avatars.mds.yandex.net/get-mpic/4033296/img_id163612759520252929.jpeg/13hq", "https://avatars.mds.yandex.net/get-mpic/4410238/img_id7304284227645390425.jpeg/13hq"],
        category: 2,
    },
    {
        name: "Samsung Galaxy A12 (SM-A125) 3/32 ГБ",
        price: 150,
        sale: 10,
        desciption: `Большой 6,5-дюймовый HD+ экран с V-образным вырезом для камеры создан для полного погружения в контент. Благодаря поддержке технологии HD+ картинка на Galaxy A12 яркая и насыщенная.
        Плавные линии и эргономичный дизайн обеспечивают удобное управление и удержание смарфтона одной рукой. Galaxy A12 представлен в трех классических цветах: черном, красном и синем.
        Снимайте памятные моменты на 48-мегапиксельную основную камеру Galaxy A12. Ультраширокогоульная камера сделает захватывающие панорамные снимки, камера с датчиком глубины сфокусируется на главном, а камера для макросъемки зафиксирует мельчайшие детали и объекты. Сверхширокоугольная камера позволит запечатлеть мир во всей полноте красок благодаря широкому углу обзора.
        Аккумулятор емкостью 5000 мАч позволит играть, выходить в прямой эфир и обмениваться файлами на протяжении целого дня. А Быстрая Зарядка 15 Вт поможет не выпасть из динамичного ритма твоей жизни.`,
        comments: [{
            user: "Barbara",
            text: "Из достоинств стоит отметить относительно долгое время работы, 4 камеры, хотя ощущения, что это только понторезка. При широком угле качество съемки посредственное, при обычном неплохо, но хромает стабилизация. Но радует цена. За 12к денег все очень хорошо. Средние игры тянет, при обычной работе и просмотре видео не тормозит. Все датчики работают хорошо, включая отпечаток распознавание лица."
        }, {
            user: "Alex",
            text: "недостаточно чувствительный экран, тяжеловат"
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 48,
        images: ["https://avatars.mds.yandex.net/get-mpic/5281967/img_id7647176951640074543.png/orig", "https://avatars.mds.yandex.net/get-mpic/5281967/img_id7647176951640074543.png/orig", "https://avatars.mds.yandex.net/get-mpic/5177644/img_id6353577842473053107.png/orig"],
        category: 2,
    },
    {
        name: "Samsung Galaxy A22 4/64 ГБ",
        price: 190,
        sale: 10,
        desciption: `Компания Samsung продолжает радовать пользователей принципом идеального соотношения цена-качество. Смартфон Samsung Galaxy A22 тому подтверждение. Обновление экрана с частотой 90 Гц, камера 48 Мп с оптической стабилизацией и батарея на 5000 мАч - всё это уже может быть доступно для вас.`,
        comments: [{
            user: "Artem",
            text: "Хотелось бы разрешение экрана full hd."
        }, {
            user: "Мария Прокопова",
            text: "Быстрый, четкий, идеальный вариант из среднего сегмента, ребенок счастлив,достаточно быстрый, игрушки работают, камера хорошая."
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 48,
        images: ["https://avatars.mds.yandex.net/get-mpic/5207439/img_id3853786263857298019.png/orig", "https://avatars.mds.yandex.net/get-mpic/4441663/img_id299754881764911656.jpeg/orig", "https://avatars.mds.yandex.net/get-mpic/4465918/img_id5605021649828934961.jpeg/orig"],
        category: 2,
    },
    {
        name: "Samsung Galaxy A03s",
        price: 300,
        sale: 50,
        desciption: `Расширь границы доступного с 6,5-дюймовым экраном с V-образным вырезом. Благодаря технологии HD+ дисплей Galaxy A03s демонстрирует яркую, четкую и чистую картинку. Galaxy A03s сочетает классический выдержанный стиль и мягкие тактильные ощущения. Улучшенная эргономика обеспечивает удобный хват и легкую навигацию по всей поверхности дисплея. Лови момент, используя 13-мегапиксельную основную камеру. Дополнительная камера с датчиком глубины поможет сфокусироваться на главном, а камера для макросъемки зафиксирует мельчайшие детали.`,
        comments: [{
            user: "Алексей Р.",
            text: "Качество соответствует ожидаемому."
        }, {
            user: "Андрей",
            text: "цена производительность на высоте, ничего не зависает, никаких подтормаживаний интерфейса."
        }],
        isFavourite: false,
        isInCart: false,
        saleTime: 24,
        images: ["https://avatars.mds.yandex.net/get-mpic/5307186/img_id2535104419673561047.png/13hq", "https://avatars.mds.yandex.net/get-mpic/5334818/img_id7367024609946977229.jpeg/13hq", "https://avatars.mds.yandex.net/get-mpic/5236535/img_id7079035875759718674.jpeg/13hq", "https://avatars.mds.yandex.net/get-mpic/5274753/img_id1246062981211785974.jpeg/13hq"],
        category: 2,
    }, {
        name: 'Buds',
        price: 150,
        sale: 5,
        description: 'Беспроводные наушники Galaxy Buds',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 36,
        images: ['https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-LjxOu_c2x-1uq7pTnrSvIsS6S6u6O5AyrjiaC5WIkLBVkZjjspQH9mXJ3wMCGopX_NI&usqp=CAU'],
        category: 3,
    },
    {
        name: 'AirPods',
        price: 180,
        sale: 10,
        description: 'Беспроводные наушники AirPods',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 12,
        images: ['https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSChbxI1xyN6E7Ha0iQ13cuqkIoVUNEWCFa4MeqdnL_bpwpP-DPXhKVi-3YKtSKgQdqdUM&usqp=CAU'],
        category: 3,
    },
    {
        name: 'Case',
        price: 5,
        sale: 0,
        description: 'Чехол для iPhone 12 mini',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 0,
        images: ['https://www.mytrendyphone.eu/images/Genuine-iPhone-12-Mini-Apple-Silicone-Case-with-MagSafe-MHKX3ZM-A-Black-0194252168851-16102020-01-p.jpg'],
        category: 3,
    },
    {
        name: 'Charger',
        price: 20,
        sale: 2,
        description: 'Без проводная, быстрая зарядка',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 72,
        images: ['https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MHXH3_AV4_GEO_MY?wid=1144&hei=1144&fmt=jpeg&qlt=80&.v=1602981199000'],
        category: 3,
    },
    {
        name: 'VRbox',
        price: 120,
        sale: 5,
        description: 'Очки вертуальной реальности',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://avatars.mds.yandex.net/get-mpic/5145248/img_id5233481511081364780.jpeg/x248_trim'],
        category: 3,
    },
    {
        name: 'iPod',
        price: 10,
        sale: 0,
        description: 'Плеер для музыки iPod Max',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 0,
        images: ['https://www.iguides.ru/upload/medialibrary/775/77568e4d44fe1c312553700398cc3830.png'],
        category: 3,
    },
    {
        name: 'Watch 7',
        price: 200,
        sale: 10,
        description: 'Смарт часы Apple Watch series 7',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 36,
        images: ['https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/apple-watch-s7-og-202110_GEO_RU?wid=1200&hei=630&fmt=jpeg&qlt=95&.v=1632423466000'],
        category: 3,
    },
    {
        name: 'iPower',
        price: 30,
        sale: 2,
        description: 'Power Bank для Apple и Android',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 24,
        images: ['https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQa71FFEXZsyY6QfgttPAkhLO_xBpk1DNi3Rw&usqp=CAU'],
        category: 3,
    },
    {
        name: 'AirPods MAX',
        price: 550,
        sale: 15,
        description: 'Беспроводные наушники AirPods MAX',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 36,
        images: ['https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/og-airpods-max-202011?wid=1200&hei=630&fmt=jpeg&qlt=95&.v=1603996970000'],
        category: 3,
    },
    {
        name: 'MI Band 6',
        price: 50,
        sale: 3,
        description: 'Спортивные часы от Xiaomi',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 168,
        images: ['https://xiaomi.uz/uploads/CatalogueImage/pvm_MiBand6%20(3)_18591_1620547556.png'],
        category: 3,
    },
    {
        name: 'Dualshock 5',
        price: 60,
        sale: 0,
        description: 'Джойстики для Play Station 5',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 0,
        images: ['https://img.manners.nl/images/p-FuIrmrJI7OnGFUxyu1tDlaPJI=/375x211/filters:quality(80):format(jpeg):background_color(fff)/https%3A%2F%2Fwww.manners.nl%2Fwp-content%2Fuploads%2F2020%2F04%2F49746632758_c50d510b77_6k.jpg'],
        category: 3,
    },
    {
        name: 'JBL Flip 4',
        price: 110,
        sale: 5,
        description: 'Беспровадная калонка JBL',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 60,
        images: ['https://m.media-amazon.com/images/I/71hiwq213qL._AC_SL1500_.jpg'],
        category: 3,
    },
    {
        name: 'Phantom RGB',
        price: 70,
        sale: 0,
        description: 'Клавиатура с подцветкой',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 0,
        images: ['https://m.media-amazon.com/images/I/71y73nqoHqL._AC_SL1500_.jpg'],
        category: 3,
    },
    {
        name: 'aTech',
        price: 10,
        sale: 0,
        description: 'Беспроводная мышка',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 0,
        images: ['https://img.ktc.ua/img/base/1/8/274798.jpg'],
        category: 3,
    },
    {
        name: 'Pebble V2',
        price: 30,
        sale: 2,
        description: 'Калонка для ПК',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 12,
        images: ['https://m.media-amazon.com/images/S/aplus-media/sc/cd17a66a-5bd2-4ae8-8925-ce30cb8baaa0.__CR0,0,970,600_PT0_SX970_V1___.jpg'],
        category: 3,
    },
    {
        name: 'Pebble V2',
        price: 30,
        sale: 2,
        description: 'Калонка для ПК',
        comments: [{
            user: '',
            text: ''
        }],
        isFavourite: false,
        isInCart: false,
        sale_time: 12,
        images: ['https://m.media-amazon.com/images/S/aplus-media/sc/cd17a66a-5bd2-4ae8-8925-ce30cb8baaa0.__CR0,0,970,600_PT0_SX970_V1___.jpg'],
        category: 2,
    }
]

export let mapped_categories = categories.map((item) => {
    item.count = products.filter(product => product.category == item.id).length

    return item
})

let products_id = products

for (let product of products_id) {
    product.id = products.indexOf(product)

    if (localStorage.cart.split(',').filter(item => item == product.id).length) product.isInCart = true
    else product.isInCart = false

    if (localStorage.liked.split(',').filter(item => item == product.id).length) product.isFavourite = true
    else product.isFavourite = false
}

console.log(products_id.map(item => item.isInCart));

export {
    products_id
}