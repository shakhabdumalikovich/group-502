import {
    products_id,
    mapped_categories
} from './data.js'

let cart_for_local = localStorage.cart.split(',') || []
let liked_for_local = localStorage.liked.split(',') || []

let box = document.querySelector('.box')
let heart_num = document.querySelector('.heart-num')
let cart_num = document.querySelector('.cart-num')
let show_catogies = document.querySelector('#show-catogies')
let forAddFavorite = []
let none_find = document.querySelector('.none-find')
let show_btn = document.querySelector('.show_btn')
let counterMin = 10
let counterMax = 10
let heart_box = document.querySelector('.heart_box')
let cart_box = document.querySelector('.cart_box')
let heart_box_arr = []
let cart_box_arr = []

show_btn.onclick = () => {
    counterMin += counterMax
    let show = products_id.slice(0, counterMin)
    if (counterMin > products_id.length) {
        show_btn.innerText = "больше нет товаров"
        show_btn.style.display = 'none'
    } else {
        reload(show)
    }
    if (show.length == products_id.length) {
        show_btn.style.display = 'none'
    }
}
// onclick favorite
const addFarvorite = (idElem) => {
    // ADD TO LOCAL
    liked_for_local.push(idElem)
    localStorage.liked = liked_for_local

    let find = products_id.filter(item => item.id == idElem)[0]
    find.isFavorite = !find.isFavorite

    if (forAddFavorite.length == 0) {
        reload(products_id)
        header_reload()
    } else reload(forAddFavorite)

}

const addCart = (id) => {
    // ADD TO LOCAL
    cart_for_local.push(id)
    localStorage.cart = cart_for_local

    let find = products_id.filter(item => item.id == id)[0]
    find.isInCart = !find.isInCart

    if (forAddFavorite.length == 0) {
        reload(products_id)
        header_reload()
    } else reload(forAddFavorite)

    if (cart_box_arr.length == 0) cart_box.innerText = 'нет товаров'
    console.log(cart_box_arr);
}

// heart_num.innerText = products_id.filter(item => item.isFavorite == true).length
// cart_num.innerText = products_id.filter(item => item.isInCart == true).length

let reload = (details) => {
    box.innerHTML = ''

    for (let item of details) {
        let a = document.createElement('a')
        let main_item = document.createElement('div')
        let top_item = document.createElement('div')
        let bottom_item = document.createElement('div')
        let h3_bottom = document.createElement('h4')
        let h4_bottom = document.createElement('h5')
        let p_bottom = document.createElement('p')
        let add_div = document.createElement('div')
        let div_cart = document.createElement('div')
        let img_cart = document.createElement('span')
        let div_fav = document.createElement('div')
        let img_fav = document.createElement('img')

        h3_bottom.innerHTML = item.name
        h4_bottom.innerHTML = '$' + item.price
        p_bottom.innerHTML = 'Продано: ' + Math.floor(Math.random() * 1000);
        img_fav.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg"  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="black" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>`
        for (let img of item.images) {
            top_item.style.backgroundImage = `url(${img})`
        }

        a.setAttribute('href', `./product.html?id=${item.id}`)
        a.setAttribute('target', '_blank')

        bottom_item.classList.add('bottom')
        top_item.classList.add('top')
        a.classList.add('item')
        div_fav.classList.add('item_add', "favorite")
        div_cart.classList.add('item_add', "cart")
        add_div.classList.add('add')

        item.isInCart ? img_cart.setAttribute('src', './static/Picture/bag-black.png') : img_cart.setAttribute('src', '../static/Picture/shopping-bag.svg')

        item.isFavorite ? img_fav.style.fill = 'black' : img_fav.style.fill = 'none'

        div_fav.append(img_fav)
        div_cart.append(img_cart)
        add_div.append(div_cart, div_fav)
        top_item.append(add_div)
        bottom_item.append(h3_bottom, h4_bottom, p_bottom)

        a.append(top_item, bottom_item)
        box.append(a)

        // onclick
        div_fav.onclick = () => {
            event.preventDefault()
            addFarvorite(item.id)
        }
        div_cart.onclick = () => {
            event.preventDefault()
            addCart(item.id)
        }
    }
}

reload(products_id.slice(0, counterMin))

// SHOW CATEGORIES START


let reload_categories = (details) => {
    show_catogies.innerHTML = ''

    for (let item of details) {
        let li = document.createElement('li')

        li.innerText = item.title + ' - ' + item.count
        li.id = item.id
        show_catogies.append(li)

        li.onclick = () => {
            SetFilter('category', item.id)
            for (const item of show_catogies.children) {
                item.classList.remove('active')
            }
            li.classList.add('active')
            forAddFavorite = []
            for (const item2 of show_catogies.children) {
                if (item2.classList.contains('active')) {
                    for (const item of products_id) {
                        if (item2.id == item.category) {
                            forAddFavorite.push(item)
                        }
                    }
                }
            }
        }
    }
}

reload_categories(mapped_categories)
// SHOW CATEGORIES END

let SetFilter = (a, b) => {
    reload(products_id.filter(item => item[a] == b))
}

// SEARCH ELEMENTS START
let search_element = document.querySelector('#search-element')

if (localStorage.search) {
    search_element.value = localStorage.search

    reload(products_id.filter(item => item.name.trim().toLowerCase().includes(localStorage.search)))
}

search_element.onkeyup = () => {
    document.querySelector('.none-find').style.display = 'none'

    // Save to localstorage
    localStorage.search = event.target.value.trim()
    forAddFavorite = []
    if (event.target.value.trim().length >= 2) {
        let find = products_id.filter(item => item.name.trim().toLowerCase().includes(event.target.value.trim().toLowerCase()))
        forAddFavorite = find

        if (find.length == 0) {
            none_find.style.display = 'block'
        }
        reload(find)

    } else {
        reload(products_id)
    }

    localStorage.search = event.target.value.trim()
    if (event.target.value.trim().length >= 2) {
        let find = products_id.filter(item => item.name.trim().toLowerCase().includes(event.target.value.trim().toLowerCase()))
        forAddFavorite = find
        reload(find)

        if (find.length == 0) {
            document.querySelector('.none-find').style.display = 'block'
        }
    }
}

// SEARCH ELEMENTS END

const header_reload = () => {
    heart_box.innerHTML = ''
    cart_box.innerHTML = ''

    heart_box_arr = []
    cart_box_arr = []

    for (const item of products_id) {
        if (item.isFavorite) {
            let heart_item = document.createElement('a')
            let heart_img = document.createElement('img')
            let heart_name = document.createElement('p')
            let heart_price = document.createElement('p')

            heart_item.classList.add('heart_item')
            heart_img.classList.add('heart_img')
            heart_name.classList.add('heart_name')
            heart_price.classList.add('heart_price')

            heart_item.setAttribute('href', `./product.html?id=${item.id}`)
            heart_img.setAttribute('src', item.images[0])
            heart_name.innerText = item.name
            heart_price.innerText = item.price

            heart_item.append(heart_img, heart_name, heart_price)
            heart_box.append(heart_item)

            heart_box_arr.push(item)
        }
        if (item.isInCart) {
            let heart_item = document.createElement('a')
            let heart_img = document.createElement('img')
            let heart_name = document.createElement('p')
            let heart_price = document.createElement('p')

            heart_item.classList.add('heart_item')
            heart_img.classList.add('heart_img')
            heart_name.classList.add('heart_name')
            heart_price.classList.add('heart_price')

            heart_item.setAttribute('href', `./product.html?id=${item.id}`)
            heart_img.setAttribute('src', item.images[0])
            heart_name.innerText = item.name
            heart_price.innerText = item.price

            heart_item.append(heart_img, heart_name, heart_price)
            cart_box.append(heart_item)

            cart_box_arr.push(item)
        }
        if (heart_box_arr.length == 0) heart_box.innerText = 'нет товаров'
        if (cart_box_arr.length == 0) cart_box.innerText = 'нет товаров'
    }
}

header_reload()