import {
   products_id
} from './data.js'

let route = window.location.search
let id = route.split('=')[1]

let product = products_id.filter(item => item.id == id)[0]

let name_prod = document.querySelector('.title h2')
let price = document.querySelector('.price p')
let heart_img = document.querySelector('.heart img')
let heart_num = document.querySelector('.heart-num')
let cart_num = document.querySelector('.cart-num')
let learn_more = document.querySelector('.learn-more')

let heart_box = document.querySelector('.heart_box')
let cart_box = document.querySelector('.cart_box')
let heart_box_arr = []
let cart_box_arr = []

heart_img.onclick = () => {
   product.isFavorite = !product.isFavorite
   favorite_reload(product)
   header_reload()

   heart_num.innerText = products_id.filter(item => item.isFavorite == true).length
}

learn_more.onclick = () => {
   product.isInCart = !product.isInCart
   cart_reload(product)
   header_reload()

   cart_num.innerText = products_id.filter(item => item.isInCart == true).length
}

heart_num.innerText = products_id.filter(item => item.isFavorite == true).length
cart_num.innerText = products_id.filter(item => item.isInCart == true).length


let reload = (details) => {
   name_prod.innerText = details.name
   price.innerHTML = '$' + details.price
}
reload(product)

let img = document.querySelector('.product')

let img_reload = (details) => {


   for (let images_all of details.images) {
      let img_block = document.createElement('div')

      img_block.style.backgroundImage = `url(${images_all})`

      if (details.images.length == 1) {
         img_block.style.gridColumnEnd = 4
         img_block.style.gridColumnStart = 1
      }

      img_block.classList.add('prod')

      img.append(img_block)
   }



}
img_reload(product)

const favorite_reload = (details) => {

   if (details.isFavorite) {
      heart_img.setAttribute('src', './static/Picture/favorite_black.png')
   } else {
      heart_img.setAttribute('src', './static/Picture/feather-heart.png')
   }
}

const cart_reload = (details) => {
   if (details.isInCart) {
      learn_more.style.background = 'green'
   } else {
      learn_more.style.background = 'red'
   }
}
favorite_reload(product)
cart_reload(product)

const header_reload = () => {
   heart_box.innerHTML = ''
   cart_box.innerHTML = ''

   heart_box_arr = []
   cart_box_arr = []

   for (const item of products_id) {
      if (item.isFavorite) {
         let heart_item = document.createElement('a')
         let heart_img = document.createElement('img')
         let heart_name = document.createElement('p')
         let heart_price = document.createElement('p')

         heart_item.classList.add('heart_item')
         heart_img.classList.add('heart_img')
         heart_name.classList.add('heart_name')
         heart_price.classList.add('heart_price')

         heart_item.setAttribute('href', `./product.html?id=${item.id}`)
         heart_img.setAttribute('src', item.images[0])
         heart_name.innerText = item.name
         heart_price.innerText = item.price

         heart_item.append(heart_img, heart_name, heart_price)
         heart_box.append(heart_item)

         heart_box_arr.push(item)
      }
      if (item.isInCart) {
         let heart_item = document.createElement('a')
         let heart_img = document.createElement('img')
         let heart_name = document.createElement('p')
         let heart_price = document.createElement('p')

         heart_item.classList.add('heart_item')
         heart_img.classList.add('heart_img')
         heart_name.classList.add('heart_name')
         heart_price.classList.add('heart_price')

         heart_item.setAttribute('href', `./product.html?id=${item.id}`)
         heart_img.setAttribute('src', item.images[0])
         heart_name.innerText = item.name
         heart_price.innerText = item.price

         heart_item.append(heart_img, heart_name, heart_price)
         cart_box.append(heart_item)

         cart_box_arr.push(item)
      }
      if (heart_box_arr.length == 0) heart_box.innerText = 'нет товаров'
      if (cart_box_arr.length == 0) cart_box.innerText = 'нет товаров'
   }
}

header_reload()